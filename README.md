# Документация Ulacos

В этой репозитории хранится документация Ulacos.

http://doc.ulacos.com

## О проекте

Не возможно осуществить хороший проект, без хорошей документации. При чем писаться она должна с первого дня. Мы стараемся документировать все что мы пишем, в тот же день когда это попадает в сам Ulacos.

## Устанвока с нуля

pip install --upgrade pip
pip install mkdocs
pip install mkdocs-material
pip install pymdown-extensions


python -m mkdocs serve
python -m mkdocs build



Документация создается на основе движка автоматической генерации статических сайтов на базе файлов формата Markdown - [MkDocs](http://www.mkdocs.org/). Сайт размещен на службе AWS S3.